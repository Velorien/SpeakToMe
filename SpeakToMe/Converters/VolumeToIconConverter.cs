﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace SpeakToMe.Converters
{
    public class VolumeToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double volume = (double)value;

            if(volume == 0)
            {
                return "";
            }

            if(volume < 0.33)
            {
                return "";
            }

            if (volume < 0.66)
            {
                return "";
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
