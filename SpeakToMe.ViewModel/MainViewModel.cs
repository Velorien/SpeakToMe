﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Media.SpeechSynthesis;
using Windows.Storage;

namespace SpeakToMe.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private VoiceInformation selectedVoice;

        private string textToSpeak;

        private string textTitle;

        private double volume;

        public MainViewModel()
        {
            Voices = SpeechSynthesizer.AllVoices.ToList();
            SelectedVoice = Voices.FirstOrDefault();
            SavedTexts = new ObservableCollection<SavedText>();
            AddNewText = new RelayCommand(AddText);
            LoadText = new RelayCommand<SavedText>(x => TextToSpeak = x.Content);
            DeleteText = new RelayCommand<SavedText>(async x => { SavedTexts.Remove(x); await SaveTexts(); if (!SavedTexts.Any()) { RaisePropertyChanged(() => SavedTexts); } });
            Volume = 1.0;
            LoadTexts();
        }

        public List<VoiceInformation> Voices { get; set; }

        public VoiceInformation SelectedVoice
        {
            get
            {
                return selectedVoice;
            }

            set
            {
                Set(() => SelectedVoice, ref selectedVoice, value);
            }
        }

        public ObservableCollection<SavedText> SavedTexts { get; set; }

        public string TextToSpeak
        {
            get
            {
                return textToSpeak;
            }

            set
            {
                Set(() => TextToSpeak, ref textToSpeak, value);
            }
        }

        public string TextTitle
        {
            get
            {
                return textTitle;
            }

            set
            {
                Set(() => TextTitle, ref textTitle, value);
            }
        }

        public double Volume
        {
            get
            {
                return volume;
            }

            set
            {
                Set(() => Volume, ref volume, value);
            }
        }

        public RelayCommand AddNewText { get; private set; }

        public RelayCommand<SavedText> LoadText { get; private set; }

        public RelayCommand<SavedText> DeleteText { get; private set; }

        public async void AddText()
        {
            SavedTexts.Add(new SavedText() { Title = TextTitle, Content = TextToSpeak });
            RaisePropertyChanged(() => SavedTexts);
            TextTitle = string.Empty;
            await SaveTexts();
        }

        public async Task SaveTexts()
        {
            var folder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await folder.CreateFileAsync("texts.xml", CreationCollisionOption.ReplaceExisting);
                using (var stream = await file.OpenStreamForWriteAsync())
                {
                    XmlSerializer xmlSerialzer = new XmlSerializer(typeof(List<SavedText>));
                    xmlSerialzer.Serialize(stream, SavedTexts.ToList());
                }
            }
            catch (IOException)
            { }
        }

        public async void LoadTexts()
        {
            var folder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await folder.GetFileAsync("texts.xml");
                using (var stream = await file.OpenStreamForReadAsync())
                {
                    XmlSerializer xmlSerialzer = new XmlSerializer(typeof(List<SavedText>));
                    SavedTexts = new ObservableCollection<SavedText>(xmlSerialzer.Deserialize(stream) as IEnumerable<SavedText>);
                }
            }
            catch (IOException)
            {
                await folder.CreateFileAsync("texts.xml");
            }
            catch(Exception ex)
            { }
        }
    }
}
